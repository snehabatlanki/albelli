﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinBin.Entities
{
    /// <summary>
    /// Order entity
    /// </summary>
    public class Order: Entity
    {
        public Order()
        {
            this.Purchases = new List<OrderItem>();
        }

        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>
        /// The order identifier.
        /// </value>
        public int OrderID { get; set; }

        /// <summary>
        /// Gets or sets the purchases.
        /// </summary>
        /// <value>
        /// The purchases.
        /// </value>
        public virtual ICollection<OrderItem> Purchases { get; set; }


        /// <summary>
        /// Gets or sets the width of the bin.
        /// </summary>
        /// <value>
        /// The width of the bin.
        /// </value>
        public decimal BinWidth { get; set; }
    }
}
