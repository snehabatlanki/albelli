﻿
namespace MinBin.Entities
{
    using MinBin.Entities.Interfaces;
    using System;

    public class Entity : IEntity
    {
        public virtual DateTime? CreatedDate { get; set; }

        public virtual DateTime? LastModifiedDate { get; set; }
    }
}
