﻿
namespace MinBin.Entities
{
    /// <summary>
    /// The Product
    /// </summary>
    /// <seealso cref="MinBin.Entities.Entity" />
    public class Product : Entity
    {
        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public decimal Width { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is stackable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is stackable; otherwise, <c>false</c>.
        /// </value>
        public bool IsStackable { get; set; }

        /// <summary>
        /// Gets or sets the stack limit.
        /// </summary>
        /// <value>
        /// The stack limit.
        /// </value>
        public int StackLimit { get; set; }

        /// <summary>
        /// Gets or sets the product description.
        /// </summary>
        /// <value>
        /// The product description.
        /// </value>
        public string ProductDescription { get; set; }
    }
}
