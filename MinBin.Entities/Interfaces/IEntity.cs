﻿
namespace MinBin.Entities.Interfaces
{
    using System;

    /// <summary>
    /// The base entity interface.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>
        /// The created date.
        /// </value>
        DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the last modified date.
        /// </summary>
        /// <value>
        /// The last modified date.
        /// </value>
        DateTime? LastModifiedDate { get; set; }
    }
}
