﻿
namespace MinBin.Processor.Interfaces
{
    using MinBin.Entities;
    using System;
    using System.Threading.Tasks;

    public interface IOrderProcessor
    {
        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <param name="productId">The order identifier.</param>
        /// <returns>The Order.</returns>
        Task<Order> GetOrderAsync(int orderId);

        /// <summary>
        /// Save Order
        /// </summary>
        /// <param name="orderId">order Identifier</param>
        /// <param name="order">order</param>
        /// <returns></returns>
        Task SaveOrder(Order order);
    }
}
