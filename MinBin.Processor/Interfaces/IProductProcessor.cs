﻿using MinBin.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MinBin.Processor.Interfaces
{
    public interface IProductProcessor
    {
        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns>The Product.</returns>
        Task<Product> GetProductAsync(int productId);
    }
}
