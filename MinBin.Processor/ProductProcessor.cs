﻿
namespace MinBin.Processor
{
    using MinBin.DataAccess.Interfaces;
    using MinBin.Entities;
    using MinBin.Processor.Interfaces;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class ProductProcessor : ProcessorBase, IProductProcessor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductProcessor"/> class.
        /// </summary>
        /// <param name="factory">The factory.</param>
        public ProductProcessor(IRepositoryFactory factory)
            : base(factory)
        {
        }

        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns>
        /// The Product.
        /// </returns>
        public Task<Product> GetProductAsync(int productId)
        {
            var repo = this.CreateRepository<Product>();
            return repo.GetByIdAsync(productId);
        }

        public Task<IEnumerable<Product>> GetProductsAsync()
        {
            var repo = this.CreateRepository<Product>();
            return repo.GetAllAsync(null);
        }
    }
}
