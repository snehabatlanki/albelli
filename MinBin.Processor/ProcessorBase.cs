﻿
namespace MinBin.Processor
{
    using MinBin.DataAccess.Interfaces;
    using MinBin.Entities.Interfaces;

    public class ProcessorBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessorBase"/> class.
        /// </summary>
        /// <param name="factory">The factory.</param>
        protected ProcessorBase(IRepositoryFactory factory)
        {
            this.RepositoryFactory = factory;
        }

        /// <summary>
        /// Gets the repository factory.
        /// </summary>
        /// <value>
        /// The repository factory.
        /// </value>
        protected IRepositoryFactory RepositoryFactory { get; }

        /// <summary>
        /// Creates the repository.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>
        /// The repository.
        /// </returns>
        protected IRepository<TEntity> CreateRepository<TEntity>()
            where TEntity : class, IEntity
        {
            return this.RepositoryFactory.CreateRepository<TEntity>();
        }
    }
}
