﻿
namespace MinBin.Processor
{
    using MinBin.DataAccess.Interfaces;
    using MinBin.Entities;
    using MinBin.Processor.Exceptions;
    using MinBin.Processor.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class OrderProcessor : ProcessorBase, IOrderProcessor
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderProcessor"/> class.
        /// </summary>
        /// <param name="factory">The factory.</param>
        public OrderProcessor(IRepositoryFactory factory, IUnitOfWorkFactory unitOfWorkFactory)
            : base(factory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <returns>
        /// The Product.
        /// </returns>
        public async Task<Order> GetOrderAsync(int orderId)
        {           
            var repo = this.CreateRepository<Order>();
            var orders =  await repo.GetAllAsync(x => x.OrderID == orderId, "Purchases").ConfigureAwait(false);
            return orders.FirstOrDefault();
        }

        /// <summary>
        /// Saves the order
        /// </summary>
        /// <param name="order">order</param>
        public async Task SaveOrder(Order order)
        {
            try
            {
                if(order == null)
                {
                    throw new OrderException("The order object is null");
                }

                var unitOfwork = this.unitOfWorkFactory.GetUnitOfWork();
                var orderRepository = unitOfwork.CreateRepository<Order>();
                var productRepo = this.CreateRepository<Product>();

                var products = await productRepo.GetAllAsync(x => order.Purchases.Any(y => x.ProductId == y.ProductId));
                order.BinWidth = await CalculateBinWidth(order, products);
                order.LastModifiedDate = DateTime.Now;

                orderRepository.Insert(order);
                await unitOfwork.SaveAsync(CancellationToken.None).ConfigureAwait(false);
            }
            catch(OrderException ex) 
            {
                Trace.TraceError(ex.ToString());
                throw ex;        
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
            }

        }

        /// <summary>
        /// Saves the order
        /// </summary>
        /// <param name="order">order</param>
        /// <param name="products">enumerable of products</param>
        public static async Task<decimal> CalculateBinWidth(Order order, IEnumerable<Product> products)
        {
            decimal width = 0;
            foreach (var orderItem in order.Purchases)
            {
                var product = products.FirstOrDefault(x => x.ProductId == orderItem.ProductId);
                if (orderItem.ProductId == product.ProductId)
                {
                    if (product.IsStackable == true)
                    {
                        var Count = orderItem.Quantity / product.StackLimit;
                        if (orderItem.Quantity % product.StackLimit != 0)
                        {
                            Count++;
                        }
                        width += Count * product.Width;
                    }
                    else
                    {
                        width += orderItem.Quantity * product.Width;
                    }
                }
            }

            return width;
        }
    }
}
