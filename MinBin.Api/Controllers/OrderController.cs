﻿using Microsoft.AspNetCore.Mvc;
using MinBin.Entities;
using MinBin.Processor;
using MinBin.Processor.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinBin.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        /// <summary>
        /// The order processor
        /// </summary>
        private readonly IOrderProcessor orderProcessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderController"/> class.
        /// </summary>
        /// <param name="orderProcessor">The order processor.</param>
        public OrderController(IOrderProcessor orderProcessor)
        {
            this.orderProcessor = orderProcessor;
        }

        /// <summary>
        /// Gets the order based on order ID.
        /// </summary>
        /// <param name="orderID">The order identifier.</param>
        /// <returns>order</returns>
        [HttpGet("{orderID}")]
        public async Task<ActionResult<IEnumerable<Order>>> GetAsync([FromRoute] int orderId)
        {
            var product = await this.orderProcessor.GetOrderAsync(orderId).ConfigureAwait(false);
            return new JsonResult(product);
        }       

        /// <summary>
        /// Posts the specified order.
        /// </summary>
        /// <param name="order">The order.</param>
        [HttpPost]
        public async Task Post([FromBody]Order order)
        {
            await this.orderProcessor.SaveOrder(order).ConfigureAwait(false);            
        }

    }
}
