﻿
namespace MinBin.Api.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using MinBin.Processor.Interfaces;
    using System.Threading.Tasks;

    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        /// <summary>
        /// The order processor
        /// </summary>
        private readonly IProductProcessor productProcessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderController"/> class.
        /// </summary>
        /// <param name="orderProcessor">The order processor.</param>
        public ProductController(IProductProcessor productProcessor)
        {
            this.productProcessor = productProcessor;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetAsync([FromRoute]int id)
        {
            var product = await this.productProcessor.GetProductAsync(id).ConfigureAwait(false);
            return new JsonResult(product);
        }
    }
}
