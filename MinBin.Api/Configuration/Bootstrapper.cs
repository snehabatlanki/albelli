﻿
namespace MinBin.Api.Configuration
{
    using Microsoft.Extensions.DependencyInjection;
    using MinBin.DataAccess.Interfaces;
    using MinBin.DataAccess.Sql;
    using MinBin.DataAccess.Sql.Interfaces;
    using MinBin.Processor;
    using MinBin.Processor.Interfaces;
    using MinBin.Repository;

    public static class Bootstrapper
    {
        public static void RegisterDependencies(IServiceCollection services)
        {
            services.AddSingleton<IConnectionFactory, ConnectionFactory>();

            services.AddScoped<ISqlDataContext, SqlDataContext>();
            services.AddScoped<IUnitOfWorkFactory, UnitOfWorkFactory>();
            services.AddScoped<IRepositoryFactory, RepositoryFactory>();

            services.AddScoped<IOrderProcessor, OrderProcessor>();
            services.AddScoped<IProductProcessor, ProductProcessor>();
        }
    }
}
