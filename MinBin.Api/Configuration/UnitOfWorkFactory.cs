﻿
namespace MinBin.Api.Configuration
{
    using MinBin.DataAccess;
    using MinBin.DataAccess.Interfaces;
    using MinBin.DataAccess.Sql.Interfaces;

    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        /// <summary>
        /// The data context.
        /// </summary>
        private readonly ISqlDataContext dataContext;

        /// <summary>
        /// The repository factory.
        /// </summary>
        private readonly IRepositoryFactory repositoryFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorkFactory" /> class.
        /// </summary>
        /// <param name="dataContext">The data context.</param>
        /// <param name="repositoryFactory">The repository factory.</param>
        public UnitOfWorkFactory(ISqlDataContext dataContext, IRepositoryFactory repositoryFactory)
        {
            this.dataContext = dataContext;
            this.repositoryFactory = repositoryFactory;
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <returns>
        /// The unit of work.
        /// </returns>
        public IUnitOfWork GetUnitOfWork()
        {
            return new UnitOfWork(this.dataContext, this.repositoryFactory);
        }
    }
}
