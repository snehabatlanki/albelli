﻿
namespace MinBin.Repository
{
    using MinBin.DataAccess.Interfaces;
    using MinBin.DataAccess.Sql;
    using MinBin.DataAccess.Sql.Interfaces;
    using MinBin.Entities.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class RepositoryFactory : IRepositoryFactory
    {
        /// <summary>
        /// The data context.
        /// </summary>
        private readonly ISqlDataContext dataContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryFactory" /> class.
        /// </summary>
        /// <param name="dataContext">The data context.</param>
        /// <param name="businessContext">The business context.</param>
        public RepositoryFactory(ISqlDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public IRepository<TEntity> CreateRepository<TEntity>()
            where TEntity : class, IEntity
        {
            return new Repository<TEntity>(new SqlDataAccess<TEntity>(this.dataContext));
        }
    }
}
