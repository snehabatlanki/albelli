﻿
namespace MinBin.Repository
{
    using MinBin.DataAccess.Interfaces;
    using MinBin.Entities.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
    {
        /// <summary>
        /// The data access.
        /// </summary>
        private readonly IDataAccess<TEntity> dataAccess;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{TEntity}"/> class.
        /// </summary>
        /// <param name="dataAccess">The data access.</param>
        public Repository(IDataAccess<TEntity> dataAccess)
        {
            this.dataAccess = dataAccess;
        }

        public void Delete(TEntity entity)
        {
            this.dataAccess.Delete(entity);
        }

        public Task<TEntity> GetByIdAsync(object id)
        {
            return this.dataAccess.GetByIdAsync(id);
        }

        public void Insert(TEntity entity)
        {
            this.dataAccess.Insert(entity);
        }

        public void Update(TEntity entity)
        {
            this.dataAccess.Update(entity);
        }

        public virtual Task<IEnumerable<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate, params string[] includeProperties)
        {
            return this.dataAccess.GetAllAsync(predicate, includeProperties);
        }
    }
}
