using Microsoft.VisualStudio.TestTools.UnitTesting;
using MinBin.DataAccess.Interfaces;
using MinBin.Entities;
using MinBin.Processor.Exceptions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace MinBin.Processor.Test
{
    [TestClass]
    public class OrderProcessorTest
    {
        [TestMethod]
        public async Task SaveOrder_ProvidedOrder_ShouldSave()
        {
            //Arrange
            var uowfMock = new Mock<IUnitOfWorkFactory>();
            var repfMock = new Mock<IRepositoryFactory>();
            var uowMock = new Mock<IUnitOfWork>();
            var prodRepMock = new Mock<IRepository<Product>>();
            var orderRepMock = new Mock<IRepository<Order>>();
            var products = SetUpProducts();

            uowfMock.Setup(m => m.GetUnitOfWork()).Returns(uowMock.Object);
            uowMock.Setup(m => m.CreateRepository<Order>()).Returns(orderRepMock.Object);
            repfMock.Setup(m => m.CreateRepository<Product>()).Returns(prodRepMock.Object);
            prodRepMock.Setup(m => m.GetAllAsync(It.IsAny<Expression<Func<Product, bool>>>())).Returns(Task.FromResult(products.AsEnumerable()));

            var orderProcessor = new OrderProcessor(repfMock.Object, uowfMock.Object);           
            var order = SetUpOrder(products);

            //Action
            await orderProcessor.SaveOrder(order);

            //Assert            
            orderRepMock.Verify(m => m.Insert(It.IsAny<Order>()), Times.Once());
            uowMock.Verify(m => m.SaveAsync(CancellationToken.None), Times.Once());

        }

        [TestMethod]
        [ExpectedException(typeof(OrderException))]
        public async Task SaveOrder_WithNullOrder_ShouldThrowError()
        {
            var uowMock = new Mock<IUnitOfWorkFactory>();
            var repMock = new Mock<IRepositoryFactory>();
            var orderProcessor = new OrderProcessor(repMock.Object, uowMock.Object);
            await orderProcessor.SaveOrder(null);
        }

        [TestMethod]
        public void CalculateBinWidth_WithStackablePurchases_ShouldReturnBinWidth()
        {
            //Arrange
            //Setup products
            var products = SetUpProducts();

            //SetUp Order
            var orderItemList = new List<OrderItem>();
            var sixMugsOrderItem = new OrderItem { ProductId = products.Find(p => p.ProductName == "mug").ProductId, Quantity = 6 };
            orderItemList.Add(sixMugsOrderItem);
            var order = new Order { OrderID = 1, Purchases = orderItemList };

            var binWidth = OrderProcessor.CalculateBinWidth(order, products);
            var expectedBinWidth = products.Find(p => p.ProductName == "mug").Width * 2;

            //Assert
            Assert.AreEqual(expectedBinWidth, binWidth.Result);
        }

        [TestMethod]
        public void CalculateBinWidth_WithoutStackablePurchases_ShouldReturnBinWidth()
        {
            //Arrange
            //Setup products
            var products = SetUpProducts();

            //SetUp Order
            var orderItemList = new List<OrderItem>();
            var fourPhotobookOrderItem = new OrderItem { ProductId = products.Find(p => p.ProductName == "photoBook").ProductId, Quantity = 4 };
            orderItemList.Add(fourPhotobookOrderItem);
            var order = new Order { OrderID = 1, Purchases = orderItemList };

            var binWidth = OrderProcessor.CalculateBinWidth(order, products);
            var expectedBinWidth = products.Find(p => p.ProductName == "photoBook").Width * fourPhotobookOrderItem.Quantity;

            //Assert
            Assert.AreEqual(expectedBinWidth, binWidth.Result);
        }

        [TestMethod]
        public void CalculateBinWidth_WithStackableandNonStackablePurchases_ShouldReturnBinWidth()
        {
            //Arrange
            //Setup products
            var products = SetUpProducts();

            //SetUp Order
            var orderItemList = new List<OrderItem>();
            var fourPhotobookOrderItem = new OrderItem { ProductId = products.Find(p => p.ProductName == "photoBook").ProductId, Quantity = 6 };
            var FourMugOrderItem = new OrderItem { ProductId = products.Find(p => p.ProductName == "mug").ProductId, Quantity = 4 };
            orderItemList.Add(fourPhotobookOrderItem);
            orderItemList.Add(FourMugOrderItem);
            var order = new Order { OrderID = 1, Purchases = orderItemList };

            var binWidth = OrderProcessor.CalculateBinWidth(order, products);
            var expectedBinWidth = products.Find(p => p.ProductName == "photoBook").Width * fourPhotobookOrderItem.Quantity + products.Find(p => p.ProductName == "mug").Width;

            //Assert
            Assert.AreEqual(expectedBinWidth, binWidth.Result);
        }

        [TestMethod]
        public async Task GetOrderAsync_WithValidOrderId_ShouldReturnOrder()
        {
            //Arrange
            //Setup order
            var order = SetUpOrder(SetUpProducts());
            var orderList = new List<Order>();
            orderList.Add(order);

            //Mock setup
            var orderRepMock = new Mock<IRepository<Order>>();
            var uowfMock = new Mock<IUnitOfWorkFactory>();
            var repfMock = new Mock<IRepositoryFactory>();
            var uowMock = new Mock<IUnitOfWork>();
            repfMock.Setup(m => m.CreateRepository<Order>()).Returns(orderRepMock.Object);           
            orderRepMock.Setup(m => m.GetAllAsync(It.IsAny<Expression<Func<Order, bool>>>(), It.IsAny<string>())).Returns(Task.FromResult(orderList.AsEnumerable()));
            var orderProcessor = new OrderProcessor(repfMock.Object, uowfMock.Object);

            //Action
            var result = await orderProcessor.GetOrderAsync(order.OrderID);

            //Assert
            orderRepMock.Verify(m => m.GetAllAsync(It.IsAny<Expression<Func<Order, bool>>>(), It.IsAny<string>()), Times.Once());
            Assert.IsNotNull(result);
            Assert.AreEqual(order.OrderID, result.OrderID);
        }


        private List<Product> SetUpProducts()
        {
            var mug = new Product { ProductId = 1, ProductName = "mug", IsStackable = true, StackLimit = 4, Width = 94, ProductDescription = "container to drink your favourite beverage in" };
            var photoBook = new Product { ProductId = 2, ProductName = "photoBook", IsStackable = false, StackLimit = 1, Width = 19, ProductDescription = "book of pictures" };
            var calendar = new Product { ProductId = 3, ProductName = "calendar", IsStackable = false, StackLimit = 1, Width = 10, ProductDescription = "Chart that shows days, months of a particular year" };
            var canvas = new Product { ProductId = 4, ProductName = "canvas", IsStackable = false, StackLimit = 1, Width = 16, ProductDescription = "painting board" };
            var greetingCard = new Product { ProductId = 5, ProductName = "greetingCard", IsStackable = false, StackLimit = 1, Width = 4.7M, ProductDescription = "greetings for your friends and family" };

            var productList = new List<Product>();
            productList.Add(mug);
            productList.Add(photoBook);
            productList.Add(calendar);
            productList.Add(canvas);
            productList.Add(greetingCard);

            return productList;
        }

        private Order SetUpOrder(List<Product> products)
        {
            var orderItemList = new List<OrderItem>();
            var TenMugsOrderItem = new OrderItem { ProductId = products.Find(p => p.ProductName == "mug").ProductId, Quantity = 10 };
            orderItemList.Add(TenMugsOrderItem);
            var TwoPhotoBookOrderItem = new OrderItem { ProductId = products.Find(p => p.ProductName == "photoBook").ProductId, Quantity = 2 };
            orderItemList.Add(TwoPhotoBookOrderItem);
            var FourCanvasOrderItem = new OrderItem { ProductId = products.Find(p => p.ProductName == "canvas").ProductId, Quantity = 4 };
            orderItemList.Add(TwoPhotoBookOrderItem);

            return new Order { OrderID = 1, Purchases = orderItemList };
        }

    }
}
