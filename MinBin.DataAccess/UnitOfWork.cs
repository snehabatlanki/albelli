﻿
namespace MinBin.DataAccess
{
    using MinBin.DataAccess.Interfaces;
    using MinBin.Entities.Interfaces;
    using System.Threading;
    using System.Threading.Tasks;

    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// The data context.
        /// </summary>
        private readonly IDataContext dataContext;

        /// <summary>
        /// The repository factory.
        /// </summary>
        private readonly IRepositoryFactory repositoryFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork" /> class.
        /// </summary>
        /// <param name="dataContext">The data context.</param>
        /// <param name="repositoryFactory">The repository factory.</param>
        public UnitOfWork(IDataContext dataContext, IRepositoryFactory repositoryFactory)
        {
            this.dataContext = dataContext;
            this.repositoryFactory = repositoryFactory;
        }

        public Task<int> SaveAsync(CancellationToken cancellationToken)
        {
            return this.dataContext.SaveAsync(cancellationToken);
        }

        public IRepository<TEntity> CreateRepository<TEntity>()
            where TEntity : class, IEntity
        {
            return this.repositoryFactory.CreateRepository<TEntity>();
        }

        public void Dispose()
        {
            this.dataContext.Dispose();
        }
    }
}
