﻿
namespace MinBin.DataAccess.Interfaces
{
    using MinBin.Entities.Interfaces;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Creates the repository.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>
        /// The repository.
        /// </returns>
        IRepository<TEntity> CreateRepository<TEntity>()
            where TEntity : class, IEntity;

        /// <summary>
        /// Saves the asynchronous.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>1, when saved successfully, 0 otherwise.</returns>
        Task<int> SaveAsync(CancellationToken cancellationToken);
    }
}
