﻿
namespace MinBin.DataAccess.Interfaces
{
    using MinBin.Entities.Interfaces;

    public interface IRepositoryFactory
    {
        /// <summary>
        /// Creates the repository.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>
        /// The repository.
        /// </returns>
        IRepository<TEntity> CreateRepository<TEntity>()
            where TEntity : class, IEntity;
    }
}
