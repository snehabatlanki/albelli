﻿
using MinBin.Entities.Interfaces;

namespace MinBin.DataAccess.Interfaces
{
    public interface IUnitOfWorkFactory
    {
        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <returns>The unit of work.</returns>
        IUnitOfWork GetUnitOfWork();
    }
}
