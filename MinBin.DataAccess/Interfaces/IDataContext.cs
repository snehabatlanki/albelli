﻿
namespace MinBin.DataAccess.Interfaces
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IDataContext : IDisposable
    {
        Task<int> SaveAsync(CancellationToken cancellationToken);
    }
}
