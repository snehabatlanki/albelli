﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinBin.DataAccess.Interfaces
{
    public interface IConnectionFactory
    {
        string GetSqlConnectionInfo();
    }
}
