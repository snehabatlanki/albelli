﻿
namespace MinBin.DataAccess.Sql
{
    using Microsoft.EntityFrameworkCore;
    using MinBin.DataAccess.Sql.Interfaces;
    using MinBin.Entities.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class SqlDataAccess<TEntity> : ISqlDataAccess<TEntity>
        where TEntity : class, IEntity
    {
        /// <summary>
        /// The database context.
        /// </summary>
        private readonly ISqlDataContext dataContext;

        /// <summary>
        /// The data set.
        /// </summary>
        private readonly DbSet<TEntity> dataset;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlDataAccess{TEntity}" /> class.
        /// </summary>
        /// <param name="dataContext">The data context.</param>
        /// <param name="businessContext">The business context.</param>
        public SqlDataAccess(ISqlDataContext dataContext)
        {
            this.dataContext = dataContext;
            this.dataset = this.dataContext.Set<TEntity>();
        }

        public DbSet<TEntity> EntitySet()
        {
            return this.dataset;
        }

        public DbSet<T> Set<T>()
            where T : class, IEntity
        {
            return this.dataContext.Set<T>();
        }

        public void Insert(TEntity entity)
        {
            this.dataset.Add(entity);
        }

        public void Update(TEntity entity)
        {
            this.dataset.Attach(entity);
            this.dataContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            this.dataset.Remove(entity);
        }

        public Task<TEntity> GetByIdAsync(object id)
        {
            return this.dataset.FindAsync(id);
        }

        /// <summary>
        /// Gets all entities asynchronous.
        /// </summary>
        /// <param name="predicate">The predicate.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns>
        /// The list of all entities asynchronously.
        /// </returns>
        public async Task<IEnumerable<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate, params string[] includeProperties)
        {
            IQueryable<TEntity> query = this.dataset;

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            return await query.ToListAsync().ConfigureAwait(false);
        }
    }
}
