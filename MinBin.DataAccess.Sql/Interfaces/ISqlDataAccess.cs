﻿
namespace MinBin.DataAccess.Sql.Interfaces
{
    using Microsoft.EntityFrameworkCore;
    using MinBin.DataAccess.Interfaces;
    using MinBin.Entities.Interfaces;

    public interface ISqlDataAccess<TEntity> : IDataAccess<TEntity>
        where TEntity : class, IEntity
    {
        /// <summary>
        /// Sets this instance.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>The DB Set.</returns>
        DbSet<TEntity> EntitySet();

        /// <summary>
        /// Sets this instance.
        /// </summary>
        /// <typeparam name="T">The type of entity.</typeparam>
        /// <returns>The DB Set.</returns>
        DbSet<T> Set<T>()
            where T : class, IEntity;
    }
}
