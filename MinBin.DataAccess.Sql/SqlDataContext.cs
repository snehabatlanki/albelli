﻿
namespace MinBin.DataAccess.Sql
{
    using Microsoft.EntityFrameworkCore;
    using MinBin.DataAccess.Interfaces;
    using MinBin.DataAccess.Sql.Interfaces;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class SqlDataContext : DbContext, ISqlDataContext
    {
        private readonly IConnectionFactory connectionFactory;

        public SqlDataContext(IConnectionFactory connectionFactory)
        {
            this.connectionFactory = connectionFactory;
        }

        public Task<int> SaveAsync(CancellationToken cancellationToken)
        {
            this.UpdateEntries();
            return this.SaveChangesAsync(cancellationToken);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(
                   this.connectionFactory.GetSqlConnectionInfo());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);
        }

        private void UpdateEntries()
        {
            var saveTime = DateTime.UtcNow;
            foreach (var entry in this.ChangeTracker.Entries())
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("CreatedDate").CurrentValue = saveTime;
                }

                if (entry.State == EntityState.Modified || entry.State == EntityState.Deleted)
                {
                    if (entry.Metadata.FindProperty("LastModifiedDate") != null && entry.Metadata.FindProperty("LastModifiedBy") != null)
                    {
                        entry.Property("LastModifiedDate").CurrentValue = saveTime;
                    }
                }
            }
        }
    }
}
