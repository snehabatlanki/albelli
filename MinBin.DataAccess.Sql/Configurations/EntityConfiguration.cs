﻿
namespace MinBin.DataAccess.Sql.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using MinBin.Entities.Interfaces;
    using System;
    using System.Linq.Expressions;

    public abstract class EntityConfiguration<TEntity> : IEntityTypeConfiguration<TEntity>
        where TEntity : class, IEntity
    {
        /// <summary>
        /// The schema name.
        /// </summary>
        private readonly string schemaName;

        /// <summary>
        /// The identity column.
        /// </summary>
        private readonly Expression<Func<TEntity, object>> propertyExpression;

        /// <summary>
        /// The has identity.
        /// </summary>
        private readonly bool hasIdentity;

        /// <summary>
        /// The table name.
        /// </summary>
        private readonly string tableName;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityConfiguration{TEntity}"/> class.
        /// </summary>
        /// <param name="propertyExpression">The property expression.</param>
        /// <param name="schemaName">The schema name.</param>
        /// <param name="hasIdentity">if set to <c>true</c> [has identity].</param>
        protected EntityConfiguration(Expression<Func<TEntity, object>> propertyExpression, string schemaName, bool hasIdentity)
        {
            this.propertyExpression = propertyExpression;
            this.schemaName = schemaName;
            this.hasIdentity = hasIdentity;
            this.tableName = typeof(TEntity).Name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityConfiguration{TEntity}"/> class.
        /// </summary>
        /// <param name="propertyExpression">The property expression.</param>
        /// <param name="schemaName">The schema name.</param>
        /// <param name="hasIdentity">if set to <c>true</c> [has identity].</param>
        /// <param name="tableName">The table name.</param>
        protected EntityConfiguration(Expression<Func<TEntity, object>> propertyExpression, string schemaName, bool hasIdentity, string tableName)
        {
            this.propertyExpression = propertyExpression;
            this.schemaName = schemaName;
            this.hasIdentity = hasIdentity;
            this.tableName = tableName;
        }

        public void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.ToTable(this.tableName, this.schemaName);
            builder.HasKey(this.propertyExpression);

            if (this.hasIdentity)
            {
                var name = (this.propertyExpression.Body as MemberExpression)?.Member?.Name;
                builder.Property(this.propertyExpression)
                    .HasColumnName(name)
                    .HasColumnType("int")
                    .IsRequired()
                    .UseSqlServerIdentityColumn();
            }

            builder.Property(x => x.CreatedDate).HasColumnName(@"CreatedDate").HasColumnType("datetime").IsRequired();
            builder.Property(x => x.LastModifiedDate).HasColumnName(@"LastModifiedDate").HasColumnType("datetime");

            this.DoConfigure(builder);
        }

        /// <summary>
        /// Does the configure.
        /// </summary>
        /// <param name="builder">The builder.</param>
        protected abstract void DoConfigure(EntityTypeBuilder<TEntity> builder);
    }
}
