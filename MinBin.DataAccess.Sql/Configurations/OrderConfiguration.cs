﻿namespace MinBin.DataAccess.Sql.Configurations
{
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using MinBin.Entities;

    class OrderConfiguration : EntityConfiguration<Order>
    {
        public OrderConfiguration()
            : base(x => x.OrderID, Constants.Schema, true, "Order")
        {
        }

        protected override void DoConfigure(EntityTypeBuilder<Order> builder)
        {
            builder.Property(x => x.OrderID).IsRequired();
            builder.Property(x => x.BinWidth).IsRequired().HasMaxLength(100);
        }
    }    
    
}
