﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MinBin.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MinBin.DataAccess.Sql.Configurations
{
    class OrderItemConfiguration : EntityConfiguration<OrderItem>
    {
        public OrderItemConfiguration()
            : base(x => x.Id, Constants.Schema, true, "OrderItem")
        {
        }

        protected override void DoConfigure(EntityTypeBuilder<OrderItem> builder)
        {
            builder.Property(x => x.OrderId).IsRequired().HasMaxLength(25);
            builder.Property(x => x.ProductId).HasMaxLength(100);
            builder.Property(x => x.Quantity).HasMaxLength(50);
            builder.HasOne(t => t.Order)
                    .WithMany(p => p.Purchases)
                    .HasForeignKey(d => d.OrderId)
                    .IsRequired();

        }
    }
}
