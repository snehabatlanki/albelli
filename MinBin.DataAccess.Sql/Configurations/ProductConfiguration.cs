﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MinBin.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MinBin.DataAccess.Sql.Configurations
{
    public class ProductConfiguration : EntityConfiguration<Product>
    {
        public ProductConfiguration()
            : base(x => x.ProductId, Constants.Schema, true, "Product")
        {
        }

        protected override void DoConfigure(EntityTypeBuilder<Product> builder)
        {
            builder.Property(x => x.ProductName).IsRequired().HasMaxLength(25);
            builder.Property(x => x.ProductDescription).HasMaxLength(100);
            builder.Property(x => x.IsStackable);
            builder.Property(x => x.StackLimit);
        }
    }
}
